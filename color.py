#!/bin/env python2

import collections
def rgb_to_rgbw(r,g,b):
    r = r*255.0
    g = g*255.0
    b = b*255.0
    low = min(r, g, b)
    high = max(r, g, b)
    saturation = round(100.0 * ((high-low)/high))
    w = (255.0-saturation) / 255.0 * (r+g+b) / 3.0
    return map(lambda x: x/255.0, [r,g,b,w]) 


def getRGBW(color):
    if not isinstance(color, collections.Sequence):
        return False
    
    rgbw = [0.0]*4

    if len(color) == 1:
        # single len array is assumed brightness.
        # we will put this on all channels for now
        rgbw = [color[0]]*4    
    elif len(color) == 3:
        # three elements are assumed to be rgb 
        # we will just convert this to rgbw
        rgbw = rgb_to_rgbw(*color)
    elif len(color) == 4:
        rgbw = color
    else:
        return False

    return rgbw


