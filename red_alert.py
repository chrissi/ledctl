from pca9485 import Pca
from time import sleep

p = Pca()

wait = 0.3
fade = 0.2
a = 1.0
while 1:
  p.fadeDCs(0,[0,0,0,a]*3,fade)
  sleep(wait)
  p.fadeDCs(0,[0]*12,fade)
  sleep(wait)

