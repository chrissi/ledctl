#!/usr/bin/env python2

from flask import Flask
import sys
app = Flask(__name__)
import json
from color import getRGBW
from  pca9485 import Pca
import time


# Shows some help and links 
@app.route('/')
def status():
    return "Status"

#uses a fuction on a zone
#usually sets a color or preset
@app.route("/<area>/<zone>/<function>")
def function(area, zone, function):
    try:
        #check if function is sane
        isPreset = False
        colors = []
        try:
            colors = [float(x) for x in function.split(",")]
            if len(colors) not in [1, 3, 4, 16]:
                colors = []
        except:
            isPreset = True

        if isPreset == False and len(colors) == 0:
            return "Unknown function"

        if isPreset == True:
            if not user.has_key(function.lower()) and not pre.has_key(function.lower()):
                return "Undefined preset"

        #check if zone is sane
        if zone.isdigit():
            if int(zone) in range(5):
                zone = int(zone)
            else:
                return "Nicht erlaubte Zonennummer"
        else:
            if zone not in ["all"]:
                return "Unbekannte zone"

        #if even the area is sane we can try to output this
        if area in ["holodeck", "treppe"]:
            if isPreset:
                if user.has_key(function.lower()):
                    setColor(area, zone, user[function][1])
                elif pre.has_key(function.lower()):
                    setColor(area, zone, pre[function])
            else:
                setColor(area, zone, colors)
            return "Tada =)"
        else:
            return "Unbekannte area. Erlaubte Werte: holodeck, treppe"
    except:
        print sys.exc_info()[0]

@app.route("/preset")
def listPresets():
    try:
            s = ["List of presets:<br>"]
            for k,v  in user.iteritems():
                s.append( u"* {}: {}<br>".format(k, v[1]))
            for k,v  in pre.iteritems():
                s.append( u"* {}: {}<br>".format(k, v))

            return "".join(s)
    except e:
      print e


@app.route("/preset/<name>/<preset>")
def setPreset(name, preset):
    p = []
    try:
        p = [float(x) for x in preset.split(",")]
    except:
        P = []
    if len(p) == 1 or len(p) == 3 or len(p) == 4 or len(p) == 16:
        #add the new preset
        user[name.lower()] = [time.time(), p]

        #if the list is too long now we will remove the oldest
        if len(user) > 50:
            key = ""
            ti = 0
            for k,v in user.iteritems():
                if ti == 0:
                    key = k
                    ti = v[0]
                else:
                    if v[0] < ti:
                        key = k
                        ti = v[0]
            del user[key]

        try:
            with open("user.json", "w") as f:
                json.dump(user, f)
                return "Preset set =) <br><br> " + listPresets()
        except:
            print sys.exc_info()[0]
            return "Ups, storing presets failed :o" + sys.exc_info()[0]
    else:
        return "Preset needs to be in format 'white' or 'red,green,blue' or 'red,green,blue,white' with all values as float.<br><br>" + listPresets()


#outputting this to the hardware...
def setColor(area, zone, color):
    """Sets a color to a zone. If zone is an integer only the first element of color is used. If zone is 'all' as much as possible of color will be used"""
    if area == "holodeck":
        if zone == "all":
            if len(color) <= 4:
                #we need to put this color on all zones for holodeck
                rgbw = getRGBW(color)
                p.setDCs(0, rgbw*4)
            elif len(color) == 16:
                p.setDCs(0, color)
        elif zone in [1,2,3,4]:
            if len(color) <= 4:
                rgbw = getRGBW(color)
                p.setDCs((zone-1)*4, rgbw)

# load system presets
try:
    pre = {}
    with open("colors.json") as f:
        pre = json.load(f)
    print "Loaded {} pre-presets".format(len(pre))
    pre = dict([(k.lower(), v) for k,v in pre.iteritems()])

except Exception as e:
    print "Could not load user preset"
    print sys.exc_info()[0]


# load user presets
try:
    user = {}
    with open("user.json") as f:
        user = json.load(f)
    print "Loaded {} user-presets".format(len(user))
    user = dict([(k.lower(), v) for k,v in user.iteritems()])

except Exception as e:
    print "Could not load preset"
    print sys.exc_info()[0]


# initializing hardware
p = Pca()

# starting flask-server
app.run(host='0.0.0.0', port=80)
